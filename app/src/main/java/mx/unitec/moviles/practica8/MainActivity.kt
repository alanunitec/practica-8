package mx.unitec.moviles.practica8

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.weather_fragment.*
import mx.unitec.moviles.practica8.model.Coordenada
import mx.unitec.moviles.practica8.weather.WeatherFragment
import java.util.*

const val REQUEST_LOCATION_PERMISSION = 2

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, WeatherFragment.newInstance())
                .commitNow()
        }

    }

    fun clickButtonLocation(view: View) {

    }

}