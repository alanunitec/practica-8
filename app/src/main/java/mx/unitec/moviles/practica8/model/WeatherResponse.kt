package mx.unitec.moviles.practica8.model

import com.squareup.moshi.Json

data class WeatherResponse(
    var coord: Coord? = null,
    var weather: List<Weather>,
    var base: String? = null,
    var main: Main? = null,
    var visibility: Float = 0.toFloat(),
    var wind: Wind? = null,
    var clouds: Clouds? = null,
    var dt: Float = 0.toFloat(),
    var sys: Sys? = null,
    var timezone: Float = 0.toFloat(),
    var id: Int = 0,
    var name: String? = null,
    var cod: Float = 0.toFloat(),
    var rain: Rain? = null
)

data class Weather(
    var id: Int = 0,
    var main: String? = null,
    var description: String? = null,
    var icon: String? = null
)

data class Clouds (
    @Json(name ="all")
    var all: Float = 0.toFloat()
)

data class Rain (
    var h3: Float = 0.toFloat()
)

data class Wind (
    var speed: Float = 0.toFloat(),
    var deg: Float = 0.toFloat()
)

data class Main (
    var temp: Float = 0.toFloat(),
    var feels_like: Float = 0.toFloat(),
    var temp_min: Float = 0.toFloat(),
    var temp_max: Float = 0.toFloat(),
    var pressure: Float = 0.toFloat(),
    var humidity: Float = 0.toFloat(),
    var sea_level: Float = 0.toFloat(),
    var grnd_level: Float = 0.toFloat()
)

data class Sys (
    var type: Int = 0,
    var id: Int = 0,
    var country: String? = null,
    var sunrise: Long = 0,
    var sunset: Long = 0
)

data class Coord (
    var lon: Float = 0.toFloat(),
    var lat: Float = 0.toFloat()
)



